# Super blockchain

## Run

- Install `Python 3.6+`
- Install dependencies `pip3 install -r requirments.txt`
- Run super blockchain node `python3 node.py <PORT>`

## Api

### Mine block

`GET /mine`

Sample request: 

`N/A`

Sample response:

```
{
    "index": 8,
    "message": "New block forged",
    "previous_hash": "7fb7803842f021e097ebdf0b0f63ec043fe6c9ab22a369b4b97f88549dbaa397",
    "proof": 51178,
    "transactions": [
        {
            "amount": 1,
            "recipient": "689d8213791b471ebc76e078edb7c8c7",
            "sender": "0"
        }
    ]
}
```

### Register nodes

`POST /nodes/register`

Sample request: 

```
{
    "nodes": ["http://127.0.0.1:5001"]
}
```

Sample response:

```
{
    "message": "New nodes have been added",
    "total_nodes": [
        "127.0.0.1:5001"
    ]
}
```

### Resolve node conflicts

`GET /nodes/resolve`

Sample request: 

`N/A`

Sample response:

```
{
    "chain": [
        {
            "index": 1,
            "previous_hash": 1,
            "proof": 100,
            "timestamp": 1536546340.424824,
            "transactions": []
        },
        {
            "index": 2,
            "previous_hash": "9fc77c349a37604d8cb73eef8ad9b7181661d40e4d04a0b68cda345c08b353f3",
            "proof": 35293,
            "timestamp": 1536546349.059673,
            "transactions": [
                {
                    "amount": 1,
                    "recipient": "90a59ff994af44e19945ca6876c3e66c",
                    "sender": "0"
                }
            ]
        },
        {
            "index": 3,
            "previous_hash": "31e5b07d1d6e1120107e5c98c6a77e14e53a484936ae430f50dad8dba98fa219",
            "proof": 35089,
            "timestamp": 1536546350.186683,
            "transactions": [
                {
                    "amount": 1,
                    "recipient": "90a59ff994af44e19945ca6876c3e66c",
                    "sender": "0"
                }
            ]
        }
    ],
    "message": "Our chain is authoritative"
}
```

### New transaction

`POST /transactions/new`

Sample request: 

```
{
	"sender": "60102dfe69404501be0aabe10e7b7e5a",
	"recipient": "some_random_address",
	"amount": 5
}
```

Sample response:

```
{
    "message": "transaction will be added to block: 3"
}
```

### Download full chain

`GET /chain`

Sample request: 

`N/A`

Sample response:

```
{
    "chain": [
        {
            "index": 1,
            "previous_hash": 1,
            "proof": 100,
            "timestamp": 1536536475.177176,
            "transactions": []
        },
        {
            "index": 2,
            "previous_hash": "f12e2fd3443f4455e279b71a82452be653f39b35eefc85efa4f887f65db138da",
            "proof": 35293,
            "timestamp": 1536536500.451503,
            "transactions": [
                {
                    "amount": 1,
                    "recipient": "689d8213791b471ebc76e078edb7c8c7",
                    "sender": "0"
                }
            ]
        },
        {
            "index": 3,
            "previous_hash": "82eee2631145fc0ea9f7c6b31af21f023571233d52f99582f969334be431f0ee",
            "proof": 35089,
            "timestamp": 1536536501.113348,
            "transactions": [
                {
                    "amount": 1,
                    "recipient": "689d8213791b471ebc76e078edb7c8c7",
                    "sender": "0"
                }
            ]
        }
    ],
    "length": 3
}
```