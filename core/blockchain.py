import requests
import hashlib
import json

from urllib.parse import urlparse
from time import time

class Blockchain(object):

    def __init__(self):
        self.chain = []
        self.current_transactions = []
        self.nodes = set()

        # Create genesis block
        self.new_block(previous_hash=1, proof=100)

    def register_node(self, address):
        """
        Adds a new node to the list of nodes

        :param address: <str> Address of a node, eg 'http://192.168.0.5:5000'
        :return: None
        """
        parsed_url = urlparse(address)
        self.nodes.add(parsed_url.netloc)

    def valid_chain(self, chain):
        """
        Determine if a given chain is valid

        :param chain: <list> Given chain to validate
        :return: <bool> True if valid, false otherwise
        """

        last_block = chain[0]
        current_index = 1

        while current_index < len(chain):
            block = chain[current_index]
            print(f'{last_block}')
            print(f'{block}')
            print('----------')

            # Check that hash of current block matches previous block
            if block['previous_hash'] != self.hash(last_block):
                return False

            # Check if proof is correct
            if not self.valid_proof(last_proof=last_block['proof'], proof=block['proof']):
                return False

            current_index += 1
            last_block = block

        return True

    def resolve_conflicts(self):
        """
        Consensus algorithm implementation: Replaces our chain with the longest on the network

        :return: <bool> True if chain was replaced and False if not
        """

        neighbours = self.nodes
        new_chain = None

        max_len = len(self.chain)

        for node in neighbours:
            response = requests.get(f'http://{node}/chain')

            if response.status_code == 200:
                node_chain_len = response.json()['length']
                node_chain = response.json()['chain']

                if node_chain_len > max_len and self.valid_chain(node_chain):
                    new_chain = node_chain
                    max_len = node_chain_len

        if new_chain:
            self.chain = new_chain
            return True

        return False

    def proof_of_work(self, last_proof):
        """
        Simple Proof of Work Algorithm:
         - Find a number p' such that hash(pp') contains leading 4 zeroes, where p is the previous p'
         - p is the previous proof, and p' is the new proof

        :param last_proof: <int> Previous proof
        :return: <int> Solution
        """
        proof = 0

        while self.valid_proof(last_proof=last_proof, proof=proof) is False:
            proof += 1

        return proof

    @staticmethod
    def valid_proof(last_proof, proof):
        """
        Validates the proof: Does hash(last_proof, proof) contain four leading zeroes?

        :param last_proof: <int> Previous proof
        :param proof: <int> Current proof
        :return: True if valid, false otherwise
        """

        guess = f'{last_proof}{proof}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:4] == "0000"

    def new_block(self, proof, previous_hash=None):
        """
        Creates a new block in the Blockchain
        
        :param proof: <int> Proof given by the proof of work alg
        :param previous_hash: <str> Hash of previous block
        :return: <dict> New block
        """
        
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'transactions': self.current_transactions,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1])
        }

        # Reset current transactions
        self.current_transactions = []

        self.chain.append(block)

        return block

    def new_transaction(self, sender, recipient, amount):
        """
        Creates a transaction to go in the next mined block
        
        :param sender: <str> Address of the Sender
        :param recipient: <str> Address of the Recipient 
        :param amount: <int> Amount transferred
        :return: <int> Index of the block that will hold the transaction
        """

        self.current_transactions.append({
            'sender': sender,
            'recipient': recipient,
            'amount': amount
        })

        return self.last_block['index'] + 1

    @staticmethod
    def hash(block):
        """
        Creates a hash string from block
        
        :param block: <dict> Block to hash
        :return: <str> Hashed representation of block
        """

        # Order keys in order to have hash consistent results on 1 block
        block_string = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(block_string).hexdigest()

    @property
    def last_block(self):
        return self.chain[-1]
